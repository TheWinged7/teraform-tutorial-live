
terraform {
  backend "s3" {
    bucket = "tform-state"
    key    = "global/s3/terraform.tfstate"
    region = "ap-southeast-2"

    dynamodb_table = "terraform_locks"
    encrypt        = true
  }
}

provider "aws" {
  region  = "ap-southeast-2"
  profile = "default"
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "tform-state"
  versioning {
    enabled = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_dynamodb_table" "terraform_locks" {
  name         = "terraform_locks"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }
}
