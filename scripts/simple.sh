#!/bin/bash
dynamodb_table_name="${data.terraform_remote_state.s3.outputs.dynamodb_table_name}"
echo "Hello, World. Terraform lock table is at '$dynamodb_table_name'." >> index.html
nohup busybox httpd -f -p "${var.webserver_port}" &