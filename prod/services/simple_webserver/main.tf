provider "aws" {
  region  = "ap-southeast-2"
  profile = "default"
}

module "webserver_cluster" {
  source = "git::https://gitlab.com/TheWinged7/teraform-tutorials-modules.git//services/webserver-cluster?ref=v0.0.1"


  cluster_name  = "prod-webservers"
  instance_type = "t2.micro"
  min_size      = 2
  max_size      = 5
  custom_tags   = { "Environment" = "production" }
  enable_scaling = true
}
