provider "aws" {
  region  = "ap-southeast-2"
  profile = "default"
  version = "~> 2.60"
}

module "webserver_cluster" {
  source = "git::https://gitlab.com/TheWinged7/teraform-tutorials-modules.git//services/webserver-cluster?ref=v0.0.5"

  cluster_name  = "stage-webservers"
  instance_type = "t2.micro"
  min_size      = 2
  max_size      = 2
  custom_tags   = { "Environment" = "staging" }
  enable_scaling = false
}
